

USE blog_db;

-- Insert Users
INSERT INTO users(email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");

INSERT INTO users(email, password, datetime_created) VALUES ("juandelacruz", "passwordB", "2021-01-01 02:00:00");

INSERT INTO users(email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");

INSERT INTO users(email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");

INSERT INTO users(email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");


-- Insert records
INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");

INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");

INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");

INSERT INTO posts(author_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- All post with author_id 1
SELECT title FROM posts WHERE author_id = 1;

-- all users emails and datetime of creation
Select email, datetime_created FROM users;

-- Update post content
UPDATE posts SET content = "Hello to the people of the EARTH!" WHERE id = 2 AND content = "Hello Earth!";

-- Delete user
DELETE FROM users WHERE email = "johndoe@gmail.com" ;